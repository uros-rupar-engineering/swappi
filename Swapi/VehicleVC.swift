//
//  VehicleVC.swift
//  Swapi
//
//  Created by uros.rupar on 6/8/21.
//

import UIKit

class VehicleVC: UIViewController {

    var currentVehicle:Vehicle!
    
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var manufacturer: UILabel!
    
    @IBOutlet weak var cost_in_credits: UILabel!
    
    @IBOutlet weak var length: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        
      
        
    }
    
    
    
   func configure()
   {
    name.text = currentVehicle.name
    model.text = currentVehicle.model
    manufacturer.text = currentVehicle.manufacturer
    cost_in_credits.text = currentVehicle.cost_in_credits
    length.text = currentVehicle.length
    
    
   }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
