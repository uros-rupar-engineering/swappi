//
//  VehiclestableVC.swift
//  Swapi
//
//  Created by uros.rupar on 6/8/21.
//

import UIKit

class VehiclestableVC: UIViewController,UITableViewDataSource ,UITableViewDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTable.reloadData()
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var myTable: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VehicleData.dataVehicles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = VehicleData.dataVehicles[indexPath.row].name
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "details"{
            
            let controller = segue.destination as! VehicleVC
            
            controller.currentVehicle = VehicleData.dataVehicles[myTable.indexPathForSelectedRow!.row]
            
            controller.title = VehicleData.dataVehicles[myTable.indexPathForSelectedRow!.row].name
        }
    }
    

   
    override func viewDidAppear(_ animated: Bool) {
        print(VehicleData.dataVehicles)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        myTable.reloadData()
    }

    



}
